package com.stylefeng.guns.modular.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stylefeng.guns.core.base.controller.BaseController;

@Controller
public class LoginController extends BaseController{

	@RequestMapping("/login")
	public String loginIndex() {
		return "/login.html";
	}
	
	@RequestMapping("/registers")
	public String registers(){
		System.out.println("================================================");
		return "/registers.html";
	}
}
