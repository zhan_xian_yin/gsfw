$(function() {
    $('.content_top_btn>li').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
    })

    $('.sex-div').click(function() {
        $(this).find('.sex-span').addClass('active').parent().siblings().find('.sex-span').removeClass('active');
    })

    var $input = $('input');
    var submitBtn = $('.submit-btn button');

    $input.blur(function() {
        var $this = $(this);
        if ($this.val() == '' && $this.parent().find('.alertBoxs').length == 0) {
            var info = $this.attr('placeholder');
            var alertBox = $('<div></div>');
            alertBox.addClass('alertBoxs');
            alertBox.html('请输入' + info);
            $this.parent().append(alertBox);
        }
    })
    $input.keyup(function() {
        var $this = $(this);
        if ($this.val() != '' && $this.parent().find('.alertBoxs').length == 1) {
            $this.parent().find('.alertBoxs').remove();
        }
    })

    $('input[type=checkbox]').click(function() {
        if (!$(this).is(":checked")) {
            submitBtn.css({ 'background-color': '#333', 'border-color': '#333' });
        } else {
            submitBtn.css({ 'background-color': '#ca0000', 'border-color': '#ca0000' });
        }
    })

    submitBtn.click(function() {
        submitForm();
    })

    function submitForm() {
        alert('表单提交');
    }
    // for (var i = 0; i < $input.length; i++) {
    //     if ($input.eq(i).val() == '') {
    //         var info = $input.eq(i).attr('placeholder');

    //         var alertBox = $('<div></div>');
    //         alertBox.addClass('alertBoxs');
    //         alertBox.html('请输入' + info);

    //         $input.eq(i).parent().append(alertBox);
    //     }
    // }
    $('.phone input').blur(function() {
        $this = $(this);
        var phone = $this.val();
        if (!(/^1[34578]\d{9}$/.test(phone))) {
            if ($this.val() != '' && $this.parent().find('.alertBoxs').length == 0) {
                var alertBox = $('<div></div>');
                alertBox.addClass('alertBoxs');
                alertBox.html("手机号码有误，请重填");
                $this.parent().append(alertBox);
                return false;
            }

        } else {
            $this.parent().find('.alertBoxs').remove();
        }
    })

    $('.checkpass input').blur(function() {
        var $this = $(this);
        var old = $('.pass input').val();
        var news = $(this).val();
        console.log(12);
        if (old != news) {
            if ($this.val() != '' && $this.parent().find('.alertBoxs').length == 0) {
                var alertBox = $('<div></div>');
                alertBox.addClass('alertBoxs');
                alertBox.html("两次输入密码不一致");
                $this.parent().append(alertBox);
                return false;
            }

        }

    })

    $('.pass input').blur(function() {
        $this = $(this);
        if ($this.val().length < 6) {
            if ($this.val() != '' && $this.parent().find('.alertBoxs').length == 0) {
                var alertBox = $('<div></div>');
                alertBox.addClass('alertBoxs');
                alertBox.html("密码必须大于6位");
                $this.parent().append(alertBox);
                return false;
            }

        }

    })

})