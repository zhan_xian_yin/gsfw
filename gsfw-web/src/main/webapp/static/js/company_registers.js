$(function(){
	//input验证信息
	var $input = $('.registers-form input');
	$input.blur(function() {
        var $this = $(this);
        if ($this.val() == '' && $this.parent().find('.alertBoxs').length == 0) {
            var info = $this.parent().attr("node_txt");
            var alertBox = $('<div></div>');
            alertBox.addClass('alertBoxs');
            alertBox.html('请输入' + info);
            $this.parent().append(alertBox);
        }
    })
	$input.keyup(function() {
        var $this = $(this);
        if ($this.val() != '' && $this.parent().find('.alertBoxs').length == 1) {
            $this.parent().find('.alertBoxs').remove();
        }
    })
	
	
	
	$('.phone input').blur(function() {
        $this = $(this);
        var phone = $this.val();
        if (!(/^[0-9]+\-[0-9]+$/.test(phone))) {
            if($this.val() != '' && $this.parent().find('.alertBoxs').length == 0) {
                var alertBox = $('<div></div>');
                alertBox.addClass('alertBoxs');
                alertBox.html("单位号码输入有误，请重填");
                $this.parent().append(alertBox);
                return false;
            }
        } else {
            $this.parent().find('.alertBoxs').remove();
        }
    })
	
	//点击下一步之后的步骤
	var index = 0;
	var submit_btn = $('.submit .nextStep');
	var relForm = $("#relForm");
	var line = $("#moveline");
	submit_btn.click(function(){
		var $this = $(this);
		setTimeout(function(){
			if(ison($this)){
				index++;
				relForm.animate({
					left:index*(-900)+"px"
				},100);
				line.animate({
					width:(index+1)*181+"px"
				},100)
			}
		},10)
	})
	
	
	
	function ison($this){
		var all_input = $this.parent().parent().find("input");
		for(var i=0;i<all_input.length;i++){
			if(!all_input.eq(i).val()){
				alert("相关信息不能为空");
				return false;
			}
		}
		if($this.parent().parent().find(".alertBoxs").length){
			alert("相关信息填写错误，请重新填写");
			return false;
		}
		return true;
	}
	
	//点击提交事件
	var opic = $("#picupload input");
	$("#final_submit").click(function(){
		if(isSub()){
			alert("信息填写完毕，正在为您提交");
		}else{
			alert("请选择至少一项进行最后的注册");
		}
	})
	
	function isSub(){
		for(var i=0;i<2;i++){
			if(opic[i].files.length){
				return true;
			}
		}
		return false;
	}
})
